## Set up Sharding using Docker Containers

## Change all file and command 192.168.x.xxx => IP

### Config servers
Start config servers (3 member replica set)

```
fish>> cd ../sharding/
docker-compose -f config-server/docker-compose.yaml up -d
```
Initiate replica set
```
fish>> mongo mongodb://192.168.x.xxx:40001
```
```
mongos>> rs.initiate(
          {
            _id: "cfgrs",
            configsvr: true,
            members: [
              { _id : 0, host : "192.168.x.xxx:40001" },
              { _id : 1, host : "192.168.x.xxx:40002" },
              { _id : 2, host : "192.168.x.xxx:40003" }
            ]
          }
        )

mongos>> rs.status()
```

### Shard 1 servers
Start shard 1 servers (3 member replicas set)
```
fish>> docker-compose -f shard1/docker-compose.yaml up -d
```
Initiate replica set
```
fish>> mongo mongodb://192.168.x.xxx:50001
```
```
mongos>> rs.initiate(
          {
            _id: "shard1rs",
            members: [
              { _id : 0, host : "192.168.x.xxx:50001" },
              { _id : 1, host : "192.168.x.xxx:50002" },
              { _id : 2, host : "192.168.x.xxx:50003" }
            ]
          }
        )

mongos>> rs.status()
```

### Mongos Router
Start mongos query router
```
fish>> docker-compose -f mongos/docker-compose.yaml up -d
```

### Add shard to the cluster
Connect to mongos
```
fish>> mongo mongodb://192.168.x.xxx:60000
```
Add shard
```
mongos> sh.addShard("shard1rs/192.168.x.xxx:50001,192.168.x.xxx:50002,192.168.x.xxx:50003")
mongos> sh.status()
```
### Adding another shard
### Shard 2 servers
Start shard 2 servers (3 member replicas set)
```
fish>> docker-compose -f shard2/docker-compose.yaml up -d
```
Initiate replica set
```
fish>> mongo mongodb://192.168.x.xxx:50004
```
```
mongos>> rs.initiate(
          {
            _id: "shard2rs",
            members: [
              { _id : 0, host : "192.168.x.xxx:50004" },
              { _id : 1, host : "192.168.x.xxx:50005" },
              { _id : 2, host : "192.168.x.xxx:50006" }
            ]
          }
        )

mongos>> rs.status()
```
### Add shard to the cluster
Connect to mongos
```
fish>> mongo mongodb://192.168.x.xxx:60000
```
Add shard
```
mongos>> sh.addShard("shard2rs/192.168.x.xxx:50004,192.168.x.xxx:50005,192.168.x.xxx:50006")
mongos>> sh.status()
```
### config

fish>> mongo mongodb://192.168.x.xxx:60000

mongos>> use sharddemo
mongos>> db.createCollecton("user")

### share database
mongos>> sh.enableSharding("sharddemo")

### share collection
mongos>> sh.shardCollection("sharddemo.user", {"title": "hashed"})
mongos>> db.user.getShardDistribution()

### Test: bash command insert 50 collection
bash>>> for i in {1..50}; do echo -e "use sharddemo \n db.user.insertOne({\"title\": \"Spider Man $i\", \"name\": \"Say Oh Yeah\"})" | mongo mongodb://192.168.x.xxx:60000; done

### Case: db.users.count() > 0
mongos>> db.users.createIndex({"title":"hashed"})
mongos>> sh.shardCollection("sharddemo.users", {"title": "hashed"})
mongos>> db.users.getShardDistribution()
